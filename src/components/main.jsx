import React, { Component } from "react";

class Mainbody extends Component {
  /**
   *
   * @returns {*} - render
   */
  render() {
    const mainbody = (
      // <div className="main-body">
      //   <div className="side">
      //     <p>SIDEBAR</p>
      //   </div>
      //   <div className="mains">
      //     <p>
      // Lorem Ipsum is simply dummy text of the printing and typesetting
      // industry. Lorem Ipsum has been the industry's standard dummy text
      // ever since the 1500s, when an unknown printer took a galley of type
      // and scrambled it to make a type specimen book. It has survived not
      // only five centuries, but also the leap into electronic typesetting,
      // remaining essentially unchanged. It was popularised in the 1960s
      // with the release of Letraset sheets containing Lorem Ipsum passages,
      // and more recently with desktop publishing software like Aldus
      // PageMaker including versions of Lorem Ipsum.
      //     </p>
      //   </div>
      // </div>
      <div id="content">
        <div class="box">
          <p>SIDEBAR</p>
        </div>

        <div class="box1">
          <p className="lagos">
           Lagos Annual Biking event
          </p>
          <div id="sub">
            <form action="">
              <input
                type="text"
                className="race-input"
                placeholder="Subscribe to know when race event starts"
              />
              <button id="btn" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
    return <div>{mainbody}</div>;
  }
}

export default Mainbody;
