import React, { Component } from "react";

class Footer extends Component {
  /**
   *
   * @returns {*} - render
   */
  render() {
    const foot = (
      <div>
        {/* <h4>HOME</h4> */}
        <ul className="footerItems">
          <li className="footer">About us</li>
          <li className="footer">Blog</li>
          <li className="footer">Contact Us</li>
          <li className="footer">Logout</li>
        </ul>
      </div>
    );
    return <div>{foot}</div>;
  }
}

export default Footer;
