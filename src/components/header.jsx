import React, { Component } from "react";

import { Link } from "react-router-dom";;

class Navigation extends Component {
  /**
   *
   * @returns {*} - render
   */
  render() {
    const nav = (
      <div>
        {/* <h4>HOME</h4> */}
        <ul className="listItems">
          <li className="lists">
            {" "}
            <Link to="">Home</Link>
          </li>
          <li className="lists">
            <Link to="">Blog</Link>
          </li>
          <li className="lists">
            <Link to="">
              Contact Us
            </Link>
          </li>
        </ul>
      </div>
    );
    return <div>{nav}</div>;
  }
}

export default Navigation;
