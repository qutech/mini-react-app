import React from 'react';
import Header from './components/header';
import { BrowserRouter as Router } from "react-router-dom";

import Mainside from './components/sidemain';
import Footer from './components/footer';
import './App.css';

function App() {

  return (
 
      <Router>
         <div>
        <Header />
        <Mainside />
        <Footer />
         </div>
      </Router>
   
  );
}

export default App;
